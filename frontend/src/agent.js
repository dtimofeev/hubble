import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = process.env.REACT_APP_API_SCHEMA + "://"
  + process.env.REACT_APP_API_HOST + ":" + process.env.REACT_APP_API_PORT;

const responseBody = res => res.body;

const requests = {
  del: url =>
    superagent.del(`${API_ROOT}${url}`).withCredentials().then(responseBody),
  get: url =>
    superagent.get(`${API_ROOT}${url}`).withCredentials().then(responseBody),
  put: (url, body) =>
    superagent.put(`${API_ROOT}${url}`, body).withCredentials().then(responseBody),
  post: (url, body) =>
    superagent.post(`${API_ROOT}${url}`, body).withCredentials().then(responseBody)
};

const User = {
  current: () =>
    requests.get('/user'),
  login: (email, password) =>
    requests.post('/login', {email, password}),
  logout: () =>
    requests.post('/logout', {}),
  signup: (email, password) =>
    requests.post('/signup', {email, password}),
};

const Spreadsheet = {
  list: () =>
    requests.get('/spreadsheets'),
  get: spreadsheetId =>
    requests.get(`/spreadsheets/${spreadsheetId}`),
  new: () =>
    requests.post('/spreadsheets/new'),
  update: payload => {
    const url = `/spreadsheets/${payload.spreadsheetId}`;
    var data = {};
    if (typeof payload.changes !== "undefined") {
      data["changes"] = payload.changes;
    }
    if (typeof payload.name !== "undefined") {
      data["name"] = payload.name;
    }
    return requests.post(url, data);
  },
  delete: (spreadsheetId) =>
    requests.del(`/spreadsheets/${spreadsheetId}`),
}

export default {
  User: User,
  Spreadsheet: Spreadsheet,
};
