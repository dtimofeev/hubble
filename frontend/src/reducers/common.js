import {
  APP_LOAD,
  HOME_PAGE_UNLOADED,
  LOGIN,
  LOGIN_PAGE_UNLOADED,
  LOGOUT,
  NEW_SPREADSHEET,
  DELETE_SPREADSHEET,
  REDIRECT,
  SIGNUP,
  REGISTER_PAGE_UNLOADED,
} from '../constants/actionTypes';

const defaultState = {
  auth: {
    loggedIn: false,
  },
  common: {
    appLoaded: false,
    redirectTo: null,
    currentUser: null,
  },
  spreadsheet: {
    spreadsheets: [],
    grid: [],
  }
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_LOAD:
      return {
        ...state,
        appLoaded: true,
      };
    case REDIRECT:
      return {...state, redirectTo: action.redirectTo || null};
    case LOGOUT:
      return {
        ...state,
        redirectTo: '/',
        currentUser: null
      };
    case LOGIN:
    case SIGNUP:
      return {
        ...state,
        redirectTo: action.error ? null : '/dashboard',
        currentUser: action.error ? null : action.payload.email
      };
    case HOME_PAGE_UNLOADED:
    case LOGIN_PAGE_UNLOADED:
    case REGISTER_PAGE_UNLOADED:
      return {
        ...state,
        viewChangeCounter: state.viewChangeCounter + 1
      };
    case NEW_SPREADSHEET:
      return {
        ...state,
        redirectTo: action.error ? null : action.payload.spreadsheet ? `/spreadsheet/${action.payload.spreadsheet}` : null,
      };
    case DELETE_SPREADSHEET:
      return {
        ...state,
        redirectTo: action.error ? null : '/dashboard',
      };
    default:
      return state;
  }
};
