import {
  LIST_SPREADSHEETS,
  SPREADSHEEET_CELL_UPDATED,
  SPREADSHEEET_NAME_UPDATED,
  SPREADSHEET_LOAD,
  DELETE_SPREADSHEET,
} from '../constants/actionTypes';

const defaultState = {
  spreadsheets: [],
  grid:[]
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case LIST_SPREADSHEETS:
    case DELETE_SPREADSHEET:
      return {
        ...state,
        spreadsheets: action.payload.spreadsheets || null,
      };
    case SPREADSHEET_LOAD:
    case SPREADSHEEET_CELL_UPDATED:
    case SPREADSHEEET_NAME_UPDATED:
      return {
        ...state,
        grid: action.payload.grid,
        name: action.payload.name,
        errors: action.error ? action.payload.errors : null,
      }
    default:
      return state;
  }
};
