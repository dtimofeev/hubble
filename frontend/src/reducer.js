import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

import { LOGOUT } from './constants/actionTypes';
import auth from './reducers/auth';
import common from './reducers/common';
import home from './reducers/home';
import spreadsheet from './reducers/spreadsheet';

const appReducer = combineReducers({
  auth,
  common,
  home,
  spreadsheet,
  router: routerReducer
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    state = undefined;
  }

  return appReducer(state, action);
}

export default rootReducer;