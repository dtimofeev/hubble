export const nonAuthorizedPaths = [
  "/",
  "/signup",
  "/login",
];