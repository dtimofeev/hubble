import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {Button, Grid, Header, Label, Segment, Table} from 'semantic-ui-react'
import agent from '../agent';
import {
  DELETE_SPREADSHEET,
  LIST_SPREADSHEETS,
  NEW_SPREADSHEET
} from '../constants/actionTypes';
import ContainerDesktop from "./ContainerDesktop";

const mapStateToProps = state => ({
  ...state,
  spreadsheets: state.spreadsheet.spreadsheets,
});

const mapDispatchToProps = dispatch => ({
  onLoad: () =>
    dispatch({type: LIST_SPREADSHEETS, payload: agent.Spreadsheet.list()}),
  newSpreadsheet: () =>
    dispatch({type: NEW_SPREADSHEET, payload: agent.Spreadsheet.new()}),
  deleteSpreadSheet: (spreadsheetId) =>
    dispatch({type: DELETE_SPREADSHEET, payload: agent.Spreadsheet.delete(spreadsheetId)}),
});

class Dashboard extends React.Component {
  constructor() {
    super();
    this.NoSpreadsheets = this.NoSpreadsheets.bind(this);
    this.ListUserSpreadsheets = this.ListUserSpreadsheets.bind(this);
    this.DeleteSpreadSheet = this.DeleteSpreadSheet.bind(this);
  }

  componentWillMount() {
    this.props.onLoad();
  }

  NoSpreadsheets() {
    return (
      <Grid container stackable verticalAlign='middle'>
        <Grid.Row>
          <Grid.Column width={8}>
            <Header as='h3' style={{fontSize: '2em'}}>
              Warining!
            </Header>
            <p style={{fontSize: '1.33em'}}>
              You don't have any spreadsheet. Try create a first one
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  };

  DeleteSpreadSheet(spreadsheetId) {
    return () => {
      this.props.deleteSpreadSheet(spreadsheetId)
    };
  }

  ListUserSpreadsheets() {
    return (
      <Grid container stackable verticalAlign='middle'>
        <Grid.Row>
          <Button
            positive
            onClick={this.props.newSpreadsheet}
          >
            New Spreadsheet
          </Button>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Table striped>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Spreadsheet</Table.HeaderCell>
                  <Table.HeaderCell>Updated (UTC)</Table.HeaderCell>
                  <Table.HeaderCell></Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>

                {this.props.spreadsheets.map(item => (
                  <Table.Row>
                    <Table.Cell>
                      <Link to={`/spreadsheet/${item.spreadsheet}`}>{item.name}</Link>
                    </Table.Cell>
                    <Table.Cell>
                      <Label>{item.updated}</Label>
                    </Table.Cell>
                    <Table.Cell>
                      <Button
                        negative
                        size='mini'
                        onClick={this.DeleteSpreadSheet(item.spreadsheet)}
                      >
                        Delete
                      </Button>
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }


  render() {
    let grid;
    if (this.props.spreadsheets) {
      grid = this.ListUserSpreadsheets()
    } else {
      grid = this.NoSpreadsheets()
    }

    return (
      <ContainerDesktop>
        <Segment style={{padding: '8em 0em'}} vertical>
          {grid}
        </Segment>
      </ContainerDesktop>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
