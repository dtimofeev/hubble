import PropTypes from 'prop-types'
import React from 'react'
import {Container, Grid, Header, Segment} from 'semantic-ui-react'
import ContainerDesktop from "./ContainerDesktop";

const HomepageHeading = ({mobile}) => (
  <Container text>
    <Header
      as='h1'
      content='Hubble-s Challenge'
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '3em',
      }}
    />
    <Header
      as='h2'
      content='Extremely simple spreadsheets in your browser.'
      inverted
      style={{
        fontSize: mobile ? '1.5em' : '1.7em',
        fontWeight: 'normal',
        marginTop: mobile ? '0.5em' : '1.5em',
      }}
    />
  </Container>
);

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
};


const HomepageLayout = () => (
  <ContainerDesktop>
    <Segment
      inverted
      textAlign='center'
      style={{minHeight: 700, padding: '1em 0em'}}
      vertical
    >
      <HomepageHeading/>
    </Segment>
  </ContainerDesktop>
);
export default HomepageLayout;
