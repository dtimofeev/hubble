import PropTypes from 'prop-types'
import React from 'react'
import {Button, Container, Label, Menu, Segment, Visibility} from 'semantic-ui-react'
import {connect} from "react-redux";
import {LOGOUT, REDIRECT} from "../constants/actionTypes";
import agent from "../agent";
import {nonAuthorizedPaths} from "../constants/paths";

export const MENU_LOGIN_PAGE = "login";
export const MENU_SIGNUP_PAGE = "signup";
export const MENU_HOMEPAGE_PAGE = "signup";

const mapStateToProps = state => ({
  ...state,
  currentUser: state.common.currentUser
});

const mapDispatchToProps = dispatch => ({
  onLogout: () =>
    dispatch({type: LOGOUT, payload: agent.User.logout()}),
  homeRedirect: () =>
    dispatch({type: REDIRECT, redirectTo: "/"}),
});


class HeaderMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.hideFixedMenu = this.hideFixedMenu.bind(this);
    this.showFixedMenu = this.showFixedMenu.bind(this);
    this.LoggedInView = this.LoggedInView.bind(this);
    this.LoggedOutView = this.LoggedOutView.bind(this);
    this.onLogout = ev => {
      this.props.onLogout()
    };
  }

  componentWillMount() {
    const path = window.location.pathname;
    const authNeeded = !nonAuthorizedPaths.includes(path);
    if (authNeeded && !this.props.currentUser){
      this.props.homeRedirect();
    }
  }


  hideFixedMenu = () => this.setState({fixed: false});
  showFixedMenu = () => this.setState({fixed: true});

  LoggedInView = () => {
    const {fixed} = this.state;

    return (
      <Visibility
        once={false}
        onBottomPassed={this.showFixedMenu}
        onBottomPassedReverse={this.hideFixedMenu}
      >
        <Segment
          inverted
          textAlign='center'
          style={{minHeight: 10, padding: '1em 0em'}}
          vertical
        >
          <Menu
            fixed={fixed ? 'top' : null}
            inverted={!fixed}
            pointing={!fixed}
            secondary={!fixed}
            size='large'
          >
            <Container>
              <Menu.Item
                as='a'
                href='/dashboard'
              >
                Dashboard
              </Menu.Item>

              <Menu.Item position='right'>
                <Label
                  as='a'
                  href='/dashboard'
                >
                  {this.props.currentUser}
                </Label>
                <Button
                  as='a'
                  inverted={!fixed}
                  primary={fixed}
                  style={{marginLeft: '0.5em'}}
                  onClick={this.onLogout}
                >
                  Logout
                </Button>
              </Menu.Item>
            </Container>
          </Menu>
        </Segment>
      </Visibility>
    )
  };

  LoggedOutView = () => {
    const {fixed} = this.state;
    let activeHome = false;
    let activeLogin = false;
    let activeSignup = false;

    switch (this.props.activePage) {
      case MENU_HOMEPAGE_PAGE:
        this.props.active_home = true;
        break;
      case MENU_LOGIN_PAGE:
        this.props.active_login = true;
        break;
      case MENU_SIGNUP_PAGE:
        this.props.active_signup = true;
        break;
      default:
        break;
    }

    return (
      <Visibility
        once={false}
        onBottomPassed={this.showFixedMenu}
        onBottomPassedReverse={this.hideFixedMenu}
      >
        <Segment
          inverted
          textAlign='center'
          style={{minHeight: 10, padding: '1em 0em'}}
          vertical
        >
          <Menu
            fixed={fixed ? 'top' : null}
            inverted={!fixed}
            pointing={!fixed}
            secondary={!fixed}
            size='large'
          >
            <Container>
              <Menu.Item
                as='a'
                href='/'
                active={activeHome}
              >
                Home
              </Menu.Item>

              <Menu.Item position='right'>
                <Button
                  as='a'
                  href="/login"
                  inverted={!fixed}
                  active={activeLogin}
                >
                  Log in
                </Button>

                <Button
                  as='a'
                  href="/signup"
                  inverted={!fixed}
                  primary={fixed}
                  style={{marginLeft: '0.5em'}}
                  active={activeSignup}
                >
                  Sign Up
                </Button>
              </Menu.Item>
            </Container>
          </Menu>
        </Segment>
      </Visibility>
    )
  };

  render() {
    let menu;
    if (this.props.currentUser) {
      menu = this.LoggedInView()
    } else {
      menu = this.LoggedOutView()
    }

    return (
      <div>
        {menu}
      </div>

    );
  }
}

HeaderMenu.propTypes = {
  children: PropTypes.node,
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMenu);