import React from 'react'
import PropTypes from 'prop-types'
import {Responsive} from 'semantic-ui-react'
import HeaderMenu from "./HeaderMenu";


const DesktopContainer = ({children}) => (
  <Responsive minWidth={Responsive.onlyTablet.minWidth}>
    <HeaderMenu/>
    {children}
  </Responsive>
);

DesktopContainer.propTypes = {
  children: PropTypes.node,
};

export default DesktopContainer;