import React from 'react';
import {connect} from 'react-redux';
import {Grid, Input, Segment} from 'semantic-ui-react'
import ReactDataSheet from 'react-datasheet';
import 'react-datasheet/lib/react-datasheet.css';
import {
  SPREADSHEEET_CELL_UPDATED,
  SPREADSHEEET_NAME_UPDATED,
  SPREADSHEET_LOAD
} from '../constants/actionTypes';
import '../styles.css';
import agent from '../agent';
import ListErrors from './ListErrors';
import ContainerDesktop from "./ContainerDesktop";

const mapStateToProps = state => ({...state.spreadsheet});


const mapDispatchToProps = dispatch => ({
  onLoad: (spreadsheetId) =>
    dispatch({type: SPREADSHEET_LOAD, payload: agent.Spreadsheet.get(spreadsheetId)}),
  onChanges: payload =>
    dispatch({type: SPREADSHEEET_CELL_UPDATED, payload: agent.Spreadsheet.update(payload)}),
  onNameChange: (payload) =>
    dispatch({type: SPREADSHEEET_NAME_UPDATED, payload: agent.Spreadsheet.update(payload)}),
});

class Spreadsheet extends React.Component {
  constructor() {
    super();
    this.valueRenderer = this.valueRenderer.bind(this);
    this.attributesRenderer = this.attributesRenderer.bind(this);
    this.onCellsChanged = this.onCellsChanged.bind(this);
    this.onNameChange = this.onNameChange.bind(this);
    this.onContextMenu = this.onContextMenu.bind(this);
  }

  componentWillMount() {
    this.props.onLoad(this.props.match.params.id);
  }

  valueRenderer = cell => cell.value;
  dataRenderer = cell => cell.expr;
  attributesRenderer = cell => {
    return {'data-hint': cell.hint || {}}
  }

  onCellsChanged = changes => {
    this.props.onChanges({
      spreadsheetId: this.props.match.params.id,
      changes: changes,
    })
  }

  onNameChange = (_, data) => {
    this.props.onNameChange({
      spreadsheetId: this.props.match.params.id,
      name: data.value,
    })
  }

  onContextMenu = (e, cell, i, j) =>
    cell.readOnly ? e.preventDefault() : null;

  render() {
    const { name, errors, grid } = this.props;
    console.log("Spreadsheet render");
    console.log(this.props);
    return (
      <ContainerDesktop>
        <Segment>
          <Grid columns={1}>
            <Grid.Row>
              <Grid.Column>
                <Input focus fluid onChange={this.onNameChange} value={name}/>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>

                <ListErrors errors={errors}/>

                <ReactDataSheet
                  data={grid}
                  valueRenderer={this.valueRenderer}
                  dataRenderer={this.dataRenderer}
                  onContextMenu={this.onContextMenu}
                  onCellsChanged={this.onCellsChanged}
                  attributesRenderer={this.attributesRenderer}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
      </ContainerDesktop>
    );
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Spreadsheet);
