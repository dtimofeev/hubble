import {Link} from 'react-router-dom';
import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import {connect} from 'react-redux';
import {SIGNUP, REGISTER_PAGE_UNLOADED, UPDATE_FIELD_AUTH} from '../constants/actionTypes';
import {Button, Form, Grid, Header, Image, Message, Segment} from "semantic-ui-react";
import ContainerDesktop from "./ContainerDesktop";

const mapStateToProps = state => ({...state.auth});

const mapDispatchToProps = dispatch => ({
  onChangeEmail: value =>
    dispatch({type: UPDATE_FIELD_AUTH, key: 'email', value}),
  onChangePassword: value =>
    dispatch({type: UPDATE_FIELD_AUTH, key: 'password', value}),
  onSubmit: (email, password) => {
    const payload = agent.User.signup(email, password);
    dispatch({type: SIGNUP, payload})
  },
  onUnload: () =>
    dispatch({type: REGISTER_PAGE_UNLOADED})
});

class Signup extends React.Component {
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    this.submitForm = (email, password) => ev => {
      ev.preventDefault();
      this.props.onSubmit(email, password);
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
        const { email, password, errors } = this.props;

    return (
      <ContainerDesktop>
        <Grid textAlign='center' style={{height: '100vh'}} verticalAlign='middle'>
          <Grid.Column style={{maxWidth: 450}}>
            <Header as='h2' color='teal' textAlign='center'>
              <Image src='/IQlogo.png'/> Create new account
            </Header>

            <ListErrors errors={errors}/>

            <Form size='large' onSubmit={this.submitForm(email, password)}>
              <Segment stacked>
                <Form.Input
                  fluid icon='user'
                  iconPosition='left'
                  placeholder='E-mail address'
                  value={email}
                  required
                  onChange={this.changeEmail}
                />

                <Form.Input
                  fluid
                  icon='lock'
                  iconPosition='left'
                  placeholder='Password'
                  type='password'
                  value={password}
                  required
                  onChange={this.changePassword}
                />

                <Button color='teal' fluid size='large'>
                  Create an account
                </Button>
              </Segment>
            </Form>

            <Message>
              Already have an account? <Link to='/login'>Log in</Link>
            </Message>

          </Grid.Column>
        </Grid>
      </ContainerDesktop>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
