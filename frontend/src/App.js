import React from 'react';
import {connect} from 'react-redux';
import {APP_LOAD, REDIRECT} from './constants/actionTypes';
import {Route, Switch} from 'react-router-dom';
import Dashboard from './components/Dashboard';
import Login from './components/Login';
import Signup from './components/Signup';
import Spreadsheet from "./components/Spreadsheet";
import {store} from './store';
import {push} from 'react-router-redux';
import HomepageLayout from './components/HomepageLayout';

const mapStateToProps = state => {
  return {
    appLoaded: state.common.appLoaded,
    appName: state.common.appName,
    currentUser: state.common.currentUser,
    redirectTo: state.common.redirectTo
  }
};

const mapDispatchToProps = dispatch => ({
  onLoad: (payload) =>
    dispatch({type: APP_LOAD, payload, skipTracking: true}),
  onRedirect: () =>
    dispatch({type: REDIRECT})
});

class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      // this.context.router.replace(nextProps.redirectTo);
      store.dispatch(push(nextProps.redirectTo));
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    this.props.onLoad(null, null);
  }

  render() {
    if (this.props.appLoaded) {
      return (
        <Switch>
          <Route exact path="/" component={HomepageLayout}/>
          <Route path="/signup" component={Signup}/>
          <Route path="/spreadsheet/:id" component={Spreadsheet}/>
          <Route path="/login" component={Login}/>
          <Route path="/dashboard" component={Dashboard}/>
        </Switch>
      );
    }
    return (
      <HomepageLayout/>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

