import json
import pathlib


def get_schema(file_name):
    current_folder = pathlib.Path(__file__).parent.absolute()
    with open(pathlib.PurePath(current_folder, file_name)) as fp:
        return json.load(fp)


def table_changes_schema():
    return get_schema("tablechanges.json")
