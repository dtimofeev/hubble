import os

from flask import Flask
from flask_cors import CORS
from flask_restful import Api

from api.spreadsheet import SpreadsheetAPI, SpreadsheetNew, ListSpreadsheets
from api.user import UserLogin, UserLogout, UserSignup
from extensions import db, login_manager, session_interface


def create_app():
    app = Flask(__name__)

    mongo_name = os.getenv("MONGO_DB", "hubble")
    mongo_host = os.getenv("MONGO_HOST", "mongodb_container")
    app.config["MONGODB_SETTINGS"] = {
        "db": mongo_name,
        "host": mongo_host,
    }
    db.init_app(app)

    secret_key = os.getenv("SECRET_KEY", '_5#y2L"F4Q8z\n\xec]/')
    app.secret_key = bytes(secret_key, "utf-8")

    app.session_interface = session_interface

    CORS(app, supports_credentials=True)

    login_manager.init_app(app)

    api = Api(app)
    api.add_resource(SpreadsheetAPI, "/spreadsheets/<string:spreadsheet_id>")
    api.add_resource(SpreadsheetNew, "/spreadsheets/new")
    api.add_resource(ListSpreadsheets, "/spreadsheets")
    api.add_resource(UserLogin, "/login")
    api.add_resource(UserSignup, "/signup")
    api.add_resource(UserLogout, "/logout")

    return app
