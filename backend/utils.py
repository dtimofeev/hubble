from openpyxl.utils import get_column_letter, column_index_from_string
from openpyxl.utils.cell import coordinate_from_string

MAX_COLUMNS = 18278


class IncorrectColumnException(Exception):
    pass


def column_label(column: int) -> str:
    """
    Converts column index into label 1 -> 'A'
    """
    if not isinstance(column, int) or column > MAX_COLUMNS:
        raise IncorrectColumnException(f"{column} is not acceptable value")
    if column < 1:
        return 'A'
    return get_column_letter(column)


def coordinates_to_address(row, column):
    """
    Converts row and column indecies in address string (1, 2) -> 'B1'
    """
    if column < 1:
        column = 1

    if row < 1:
        row = 1

    lbl = column_label(column)
    return f'{lbl}{row}'


def address_to_coordinates(address: str):
    """
    Converts address into coordinates 'B3' -> (3,2)
    """
    col, row = coordinate_from_string(address)
    col_index = column_index_from_string(col)
    return row, col_index


def is_range(address:str):
    return ":" in address

def expand_address_range(range_address: str):
    """
    Converts given range into addresses list 'A1:A3' -> ['A1', 'A2', 'A3']
    """
    splitted = range_address.split(":")
    if len(splitted) != 2:
        return splitted

    left_row, left_column = address_to_coordinates(splitted[0])
    right_row, right_column = address_to_coordinates(splitted[1])

    if left_row > right_row:
        left_row, right_row = right_row, left_row

    if left_column > right_column:
        left_column, right_column = right_column, left_column

    addresses = list()
    for row in range(left_row, right_row + 1):
        for col in range(left_column, right_column + 1):
            addresses.append(coordinates_to_address(row, col))
    return addresses
