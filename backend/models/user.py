import flask_login
from passlib.hash import pbkdf2_sha256

from extensions import db


class User(db.Document, flask_login.UserMixin):
    email = db.StringField(primary_key=True)
    password_hash = db.StringField(required=True)
    first_name = db.StringField()
    last_name = db.StringField()
    is_authenticated = db.BooleanField()

    def get_id(self):
        return self.email

    def set_password(self, password):
        self.password_hash = pbkdf2_sha256.hash(password)

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.password_hash)
