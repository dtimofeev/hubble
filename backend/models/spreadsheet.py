import re
from datetime import datetime

import networkx as nx
from mongoengine import signals

from extensions import db
from models.user import User
from utils import address_to_coordinates, coordinates_to_address, expand_address_range

MAX_WIDTH = 10
MAX_HEIGHT = 10

CELL_STATE_NOT_INITIALIZED = 0
CELL_STATE_OK = 1
CELL_STATE_ERROR = 2

CELL_VALUE_UNKNOWN = 0
CELL_VALUE_INT = 1
CELL_VALUE_FORMULA = 2
CELL_VALUE_ERROR = 3


class Cell(db.EmbeddedDocument):
    address = db.StringField()
    state = db.IntField()
    value_type = db.IntField()
    value = db.DynamicField()
    expression = db.StringField()
    error_message = db.StringField()
    subscribers = db.ListField()  # dependatns
    dependencies = db.ListField()

    def subscribe(self, subscriber: str):
        _s = set(self.subscribers)
        _s.add(subscriber)
        self.subscribers = list(_s)

    def unsubscribe(self, subscriber: str):
        _s = set(self.subscribers)
        if subscriber in _s:
            _s.remove(subscriber)
        self.subscribers = list(_s)


SPREADSHEET_STATUS_ACTIVE = 1
SPREADSHEET_STATUS_DELETED = 2


class Spreadsheet(db.Document):
    owner = db.ReferenceField(User)
    name = db.StringField()
    width = db.IntField(default=MAX_WIDTH)
    height = db.IntField(default=MAX_HEIGHT)
    grid = db.MapField(db.EmbeddedDocumentField(Cell))
    status = db.IntField(default=SPREADSHEET_STATUS_ACTIVE)
    updated = db.DateTimeField(default=datetime.now)

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        document.updated = datetime.now()

    def extend_subscribers_graph(self, seed: str, graph: nx.DiGraph, visited:set):
        visited.add(seed)
        seeding_cell = self.grid.get(seed)
        if seeding_cell:
            for subscriber_address in seeding_cell.subscribers:
                graph.add_edge(subscriber_address, seed)
                if subscriber_address not in visited:
                    self.extend_subscribers_graph(subscriber_address, graph, visited)

    def extend_dependencies_graph(self, seed: str, graph: nx.DiGraph, visited:set):
        visited.add(seed)
        seeding_cell = self.grid.get(seed)
        if seeding_cell:
            for dependency_address in seeding_cell.dependencies:
                graph.add_edge(seed, dependency_address)
                if dependency_address not in visited:
                    self.extend_dependencies_graph(dependency_address, graph, visited)

    def evaluate(self, address: str):
        if address not in self.grid:
            return 0
        if self.grid[address].state == CELL_STATE_NOT_INITIALIZED:
            return 0
        if self.grid[address].value_type == CELL_VALUE_UNKNOWN:
            return 0
        return self.grid[address].value

    def evaluate_range(self, range_addresses: str):
        addresses = expand_address_range(range_addresses)
        if not addresses:
            return (range_addresses, self.evaluate(range_addresses))
        values = [(address, self.evaluate(address)) for address in addresses]
        return values


signals.pre_save.connect(Spreadsheet.pre_save, sender=Spreadsheet)
