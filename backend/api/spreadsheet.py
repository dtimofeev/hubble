from typing import Dict, List, TypeVar

import flask_login
import networkx as nx
from flask import request
from flask_restful import Resource
from jsonschema import validate, ValidationError
from mongoengine.errors import DoesNotExist
from pycel.excelformula import ExcelFormula, FormulaEvalError, FormulaParserError

import schemas
from models.spreadsheet import (
    Spreadsheet,
    Cell,
    SPREADSHEET_STATUS_ACTIVE,
    SPREADSHEET_STATUS_DELETED,
    CELL_STATE_NOT_INITIALIZED,
    CELL_STATE_OK,
    CELL_STATE_ERROR,
    CELL_VALUE_ERROR,
    CELL_VALUE_FORMULA,
    CELL_VALUE_INT,
    CELL_VALUE_UNKNOWN,
)
from models.user import User
from utils import coordinates_to_address, column_label, expand_address_range, is_range

T = TypeVar('T')

table_changes_shema = schemas.table_changes_schema()


class SpreadsheetAPI(Resource):
    method_decorators = [
        flask_login.login_required,
    ]

    def render_grid(self, spreadsheet: Spreadsheet) -> List[List[Dict[str, T]]]:
        """
        Render internal reprsentation of spreadsheet into datastructure for
        https://github.com/nadbm/react-datasheet
        Add:
        - Column labels into 0-row
        - Row numbers into 0-column
        - Empty cells where missing them in in table
        """
        empty_item = {
            "expr": "",
            "value": "",
        }
        represintation = list()
        columns = [{'readOnly': True, 'value': ''}]
        for col in range(1, spreadsheet.height + 1):
            columns.append({
                'value': column_label(col),
                'readOnly': True
            })
        represintation.append(columns)

        for row in range(1, spreadsheet.width + 1):
            line = list()
            line.append({'readOnly': True, 'value': row}, )
            for col in range(1, spreadsheet.height + 1):
                address = coordinates_to_address(row, col)
                if address in spreadsheet.grid:
                    cell = spreadsheet.grid[address]
                    item = {
                        "value": cell.value,
                        "expr": "",
                    }
                    if cell.value_type == CELL_VALUE_FORMULA:
                        item["expr"] = cell.expression
                    if cell.state == CELL_STATE_ERROR:
                        item["hint"] = cell.error_message
                    line.append(item)
                else:
                    line.append(empty_item)
            represintation.append(line)
        return represintation

    def render_response(
            self,
            spreadsheet: Spreadsheet,
            errors: dict = None,
            status_code: int = 200
    ):
        grid = self.render_grid(spreadsheet)
        response = {
            "grid": grid,
            "name": spreadsheet.name,
        }
        if errors:
            response["errors"] = errors
        return response, status_code

    def get(self, spreadsheet_id: str):
        try:
            spreadsheet = Spreadsheet.objects.get(
                owner=flask_login.current_user.email,
                id=spreadsheet_id,
                status=SPREADSHEET_STATUS_ACTIVE,
            )
        except DoesNotExist:
            return {"errors": {"not found": "Spreadsheet not found"}}, 421
        return self.render_response(spreadsheet)

    def post(self, spreadsheet_id: str):
        try:
            spreadsheet = Spreadsheet.objects.get(
                owner=flask_login.current_user.email,
                id=spreadsheet_id,
                status=SPREADSHEET_STATUS_ACTIVE,
            )
        except DoesNotExist:
            return {"errors": {"not found": "Spreadsheet not found"}}, 421

        try:
            data = request.json
            if "changes" in data and data["changes"]:
                validate(data, schema=table_changes_shema)

        except ValidationError as e:
            return self.render_response(
                spreadsheet=spreadsheet,
                errors={
                    "Changes validation": e.message,
                },
                status_code=422
            )

        name = data.get("name")
        if name:
            spreadsheet.name = name

        changes = data.get('changes', [])

        def evaluate(address):
            return spreadsheet.evaluate(address)

        def evaluate_range(range_address):
            return spreadsheet.evaluate_range(range_address)

        context = ExcelFormula.build_eval_context(
            evaluate=evaluate,
            evaluate_range=evaluate_range)

        updated_cells = list()
        graph = nx.DiGraph()

        for cell_udpate in changes:
            row = cell_udpate.get('row', -1)
            col = cell_udpate.get('col', -1)
            if row <= 0:
                return self.render_response(
                    spreadsheet=spreadsheet,
                    errors={
                        "error": "Row should be positive integer",
                    },
                    status_code=422
                )
            if col <= 0:
                return self.render_response(
                    spreadsheet=spreadsheet,
                    errors={
                        "error": "Col should be positive integer",
                    },
                    status_code=422
                )
            value = cell_udpate.get('value', None)

            if value is None:
                return self.render_response(
                    spreadsheet=spreadsheet,
                    errors={
                        "error": "Value should be present",
                    },
                    status_code=422
                )

            address = coordinates_to_address(row, col)
            cell = spreadsheet.grid.get(address)
            graph.add_node(address)

            if not cell:
                cell = Cell(
                    address=address,
                    state=CELL_STATE_NOT_INITIALIZED,
                    value_type=CELL_VALUE_UNKNOWN,
                )
            else:
                for dep_address in cell.dependencies:
                    dep_cell = spreadsheet.grid.get(dep_address)
                    if dep_cell:
                        dep_cell.unsubscribe(address)
                cell.dependencies = []
                cell.value = ""
                cell.value_type = CELL_VALUE_UNKNOWN
                cell.expression = ""
                cell.error_message = ""

            if value.startswith("="):
                cell.state = CELL_STATE_OK
                cell.value_type = CELL_VALUE_FORMULA
                cell.expression = value
                try:
                    formula = ExcelFormula(cell.expression)
                    dependencies = [addr.address for addr in formula.needed_addresses]

                    # Check if there are ranges in formulas
                    ranges_found = set(dep for dep in dependencies if is_range(dep))

                    # Remove ranges from dependencies
                    dependencies = set(dependencies) - ranges_found

                    # Expand ranges into cell addresses
                    dependencies = list(dependencies)
                    for dependency_range in ranges_found:
                        dependencies.extend(expand_address_range(dependency_range))

                    # Add dependencies
                    for dependency in dependencies:
                        dep_cell = spreadsheet.grid.get(dependency)
                        if not dep_cell:
                            dep_cell = Cell(
                                state=CELL_STATE_NOT_INITIALIZED,
                                value_type=CELL_VALUE_UNKNOWN,
                            )
                            spreadsheet.grid[dependency] = dep_cell
                        dep_cell.subscribe(address)

                    # Save dependencies into cell
                    cell.dependencies = dependencies
                except FormulaParserError:
                    cell.state = CELL_STATE_ERROR
                    cell.value = "#ERR"
                    cell.error_message = f"Unable parse formula {formula}"
            elif value == "":
                cell.value = ""
                cell.state = CELL_STATE_OK
                cell.value_type = CELL_VALUE_UNKNOWN
            else:
                cell.dependencies = []
                try:
                    cell.value = int(value)
                    cell.state = CELL_STATE_OK
                    cell.value_type = CELL_VALUE_INT
                except ValueError:
                    cell.state = CELL_STATE_ERROR
                    cell.value_type = CELL_VALUE_UNKNOWN
                    cell.value = "#ERR"
                    cell.error_message = "Cell should integer or formula"

            spreadsheet.grid[address] = cell
            updated_cells.append(address)

        for node_address in updated_cells:
            spreadsheet.extend_subscribers_graph(node_address, graph, set())
            spreadsheet.extend_dependencies_graph(node_address, graph, set())

        for cycle in nx.simple_cycles(graph):
            for node_address in cycle:
                if node_address not in spreadsheet.grid:
                    continue
                spreadsheet.grid[node_address].state = CELL_STATE_ERROR
                spreadsheet.grid[node_address].value = "#REFF"
                spreadsheet.grid[node_address].error_message = "Unable calculate formula"
                if node_address in graph.nodes:
                    graph.remove_node(node_address)

        walking_path = list(reversed(list(nx.topological_sort(graph))))
        for node_address in walking_path:
            cell = spreadsheet.grid.get(node_address)
            if cell and cell.value_type == CELL_VALUE_FORMULA:
                try:
                    no_dependency_errors = True
                    for dep in cell.dependencies:
                        if spreadsheet.grid[dep].state == CELL_STATE_ERROR:
                            no_dependency_errors = False

                    if no_dependency_errors:
                        formula = ExcelFormula(cell.expression)
                        value = context(formula)
                        cell.value = value
                        cell.state = CELL_STATE_OK
                    else:
                        cell.state = CELL_STATE_ERROR
                        cell.value = "#ERR"
                        cell.error_message = "Dependency error"
                except (TypeError, FormulaEvalError, FormulaParserError):
                    cell.state = CELL_STATE_ERROR
                    cell.value = "#ERR"
                    cell.error_message = f"Unable evaluate formula {cell.expression}"
                finally:
                    spreadsheet.grid[node_address] = cell

        spreadsheet.save()
        return self.render_response(spreadsheet)

    def delete(self, spreadsheet_id: str):
        try:
            spreadsheet = Spreadsheet.objects.get(
                owner=flask_login.current_user.email,
                id=spreadsheet_id,
                status=SPREADSHEET_STATUS_ACTIVE,
            )
        except DoesNotExist:
            return {"errors": {"not found": "Spreadsheet not found"}}, 421

        spreadsheet.status = SPREADSHEET_STATUS_DELETED
        spreadsheet.save()

        spreadsheets = list()
        for spreadsheet in Spreadsheet.objects(
                owner=flask_login.current_user.email,
                status=SPREADSHEET_STATUS_ACTIVE,
        ):
            spreadsheets.append(
                {
                    "spreadsheet": f"{spreadsheet.id}",
                    "name": spreadsheet.name,
                    "updated": f"{spreadsheet.updated:%Y/%m/%d %H:%M:%S}",
                }
            )

        return {"spreadsheets": spreadsheets}


class SpreadsheetNew(Resource):
    method_decorators = [
        flask_login.login_required,
    ]

    def post(self):
        user = User.objects.get(email=flask_login.current_user.email)
        spreadsheet = Spreadsheet(owner=user, name="New Spreadsheet")
        spreadsheet.save()
        return {"spreadsheet": f"{spreadsheet.id}"}


class ListSpreadsheets(Resource):
    method_decorators = [
        flask_login.login_required,
    ]

    def get(self):
        spreadsheets = list()
        for spreadsheet in Spreadsheet.objects(
                owner=flask_login.current_user.email,
                status=SPREADSHEET_STATUS_ACTIVE,
        ):
            spreadsheets.append(
                {
                    "spreadsheet": f"{spreadsheet.id}",
                    "name": spreadsheet.name,
                    "updated": f"{spreadsheet.updated:%Y/%m/%d %H:%M:%S}",
                }
            )

        return {"spreadsheets": spreadsheets}
