from email.utils import parseaddr

import flask_login
from flask import request, session
from flask_restful import Resource
from mongoengine.errors import DoesNotExist

from extensions import login_manager
from models.user import User


@login_manager.user_loader
def user_loader(email):
    user = None
    try:
        user = User.objects.get(email=email)
    except DoesNotExist:
        pass
    finally:
        return user


@login_manager.request_loader
def request_loader(request):
    email = session.get('email')
    user = None
    try:
        user = User.objects.get(email=email)
    except DoesNotExist:
        pass
    finally:
        return user


def validated_auth(data: dict) -> (dict, str, str):
    errors = {}
    email = data.get("email")
    if email:
        email = email.strip()
        parsed = parseaddr(email)
        if email != parsed[1]:
            errors["email"] = f"'{email}' is incorrectly formatted email address"
        email = parsed[1]
    elif email is None:
        errors["email"] = "Email should be presents"
    else:
        errors["email"] = "Email shouldn't be empty"

    password = data.get("password")
    if password is None:
        errors["password"] = "Password should be present into Signup request"
    elif not password:
        errors["password"] = "Password shouldn't be empty"

    return errors, email, password


class UserSignup(Resource):
    def post(self):
        errors, email, password = validated_auth(request.json)

        if errors:
            return {"errors": errors}, 422

        if User.objects(email=email):
            return {"errors": {"email": "User with this email already exists"}}, 422

        user = User(email=email)
        user.set_password(password)
        user.is_authenticated = True
        user.save()

        flask_login.login_user(user)
        session['email'] = email
        return {"message": "Signed up and Logged in", "email": email}


class UserLogin(Resource):
    def post(self):
        errors, email, password = validated_auth(request.json)

        if errors:
            return {"errors": errors}, 422

        try:
            user = User.objects.get(email=email)
        except DoesNotExist:
            return {"errors": {"login": "User not found"}}, 422

        if not user.verify_password(password):
            return {"errors": {"login": "Incorrect password"}}, 422

        user.is_authenticated = True
        user.save()
        flask_login.login_user(user)
        return {"message": "Logged in", "email": email}


class UserLogout(Resource):
    method_decorators = [
        flask_login.login_required,
    ]

    def post(self):
        email = flask_login.current_user.email
        flask_login.logout_user()
        user = User.objects.get(email=email)
        user.is_authenticated = False
        user.save()
        return {"message": "Logged out"}
