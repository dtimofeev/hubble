import flask_login

login_manager = flask_login.LoginManager()

from flask_mongoengine import MongoEngine, MongoEngineSessionInterface

db = MongoEngine()
session_interface = MongoEngineSessionInterface(db)
