import json
import os
import unittest

from flask import Response

from app import create_app
from models.spreadsheet import Spreadsheet, Cell, CELL_VALUE_INT
from models.user import User

DEFAULT_EMAIL = "test@hubble-s.com"
DEFAULT_PASSWORD = "password"


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        os.environ["MONGO_DB"] = "test"
        os.environ["MONGO_HOST"] = 'mongomock://localhost'

        app = create_app()
        self.client = app.test_client()

    def tearDown(self) -> None:
        Spreadsheet.drop_collection()
        User.drop_collection()

    def get(self, url) -> Response:
        return self.client.get(url)

    def post(self, url: str, data: dict = None) -> Response:
        if data is not None:
            data = json.dumps(data)
        return self.client.post(
            url,
            content_type='application/json',
            data=data,
        )

    def delete(self, url) -> Response:
        return self.client.delete(url)


class BaseUserTestCase(BaseTestCase):
    create_default_user = True
    login_as_default_user = True

    def setUp(self):
        super().setUp()
        if self.create_default_user:
            self.default_user = self.create_user(DEFAULT_EMAIL, DEFAULT_PASSWORD)
        if self.login_as_default_user:
            self.login(DEFAULT_EMAIL, DEFAULT_PASSWORD)

    def create_user(self, email, password: str) -> User:
        user = User(email=email)
        user.set_password(password)
        user.save()
        return user

    def login(self, email, password: str):
        data = {
            "email": email,
            "password": password,
        }

        response = self.post("/login", data)

        self.assertEqual(200, response.status_code)


class BaseSpreadSheetTestCase(BaseUserTestCase):
    def setUp(self):
        super().setUp()
        self.empty_spreadsheet = self.create_empty_spreadsheet(self.default_user)
        self.filled_spreadsheet = self.create_filled_spreadsheet(self.default_user)

    def create_empty_spreadsheet(self, user):
        spreadsheet = Spreadsheet(owner=user, name="Empty Test Speadsheet")
        spreadsheet.save()
        return spreadsheet

    def create_filled_spreadsheet(self, user):
        spreadsheet = Spreadsheet(owner=user, name="Filled Test Speadsheet")
        cell = Cell(value=1, state=CELL_VALUE_INT)
        spreadsheet.grid["A1"] = cell
        spreadsheet.save()
        return spreadsheet

    def format_spread_sheet(self, spreadsheet: Spreadsheet):
        return {
            "spreadsheet": str(spreadsheet.id),
            "name": spreadsheet.name,
            "updated": f"{spreadsheet.updated:%Y/%m/%d %H:%M:%S}",
        }
