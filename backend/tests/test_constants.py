from unittest import TestCase

from models.spreadsheet import MAX_WIDTH, MAX_HEIGHT
from utils import MAX_COLUMNS


# Catch unconscious constants altering
class ConstantsTestCase(TestCase):
    def test_max_width(self):
        self.assertEqual(10, MAX_WIDTH)

    def test_max_height(self):
        self.assertEqual(10, MAX_HEIGHT)

    def test_max_columns_for_addressing(self):
        self.assertEqual(18278, MAX_COLUMNS)
