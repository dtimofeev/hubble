from tests.basetestcase import BaseUserTestCase, DEFAULT_EMAIL, DEFAULT_PASSWORD


class BaseAuthVerificationMixin():
    def test_missing_email_returns_error(self):
        response = self.post(self.url, {
            "password": DEFAULT_PASSWORD,
        })
        self.assertEqual(422, response.status_code)

    def test_missing_password_returns_error(self):
        response = self.post(self.url, {
            "email": DEFAULT_EMAIL,
        })
        self.assertEqual(422, response.status_code)

    def test_incorrectly_formatted_email_returns_error(self):
        response = self.post(self.url, {
            "email": "test @ hubble-s.com",
            "password": DEFAULT_PASSWORD,
        })
        self.assertEqual(422, response.status_code)


class SignupTestCase(BaseUserTestCase, BaseAuthVerificationMixin):
    create_default_user = False
    login_as_default_user = False

    def setUp(self):
        super().setUp()
        self.url = '/signup'

    def test_signup(self):
        response = self.post(self.url, {
            "email": "signup@test.com",
            "password": DEFAULT_PASSWORD,
        })
        self.assertEqual(200, response.status_code)


class LoginTestCase(BaseUserTestCase, BaseAuthVerificationMixin):
    login_as_default_user = False

    def setUp(self):
        super().setUp()
        self.url = '/login'

    def test_login(self):
        self.login(DEFAULT_EMAIL, DEFAULT_PASSWORD)

    def test_disallow_login_for_non_existing_user(self):
        response = self.post(self.url, {
            "email": "unknown@unknown.com",
            "password": "password",
        })
        self.assertEqual(422, response.status_code)

    def test_wrong_password_returns_error(self):
        response = self.post(self.url, {
            "email": DEFAULT_EMAIL,
            "password": DEFAULT_PASSWORD + DEFAULT_PASSWORD,
        })
        self.assertEqual(422, response.status_code)


class LogoutTestCase(BaseUserTestCase):
    login_as_default_user = False

    def test_logout(self):
        self.login(DEFAULT_EMAIL, DEFAULT_PASSWORD)
        response = self.post('/logout')
        self.assertEquals(200, response.status_code)

    def test_logout_for_not_logged_in(self):
        response = self.post('/logout')
        self.assertEquals(401, response.status_code)
