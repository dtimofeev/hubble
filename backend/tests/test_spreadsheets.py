from models.spreadsheet import (
    MAX_HEIGHT,
    MAX_WIDTH,
    Spreadsheet,
    SPREADSHEET_STATUS_DELETED,
    SPREADSHEET_STATUS_ACTIVE,
    CELL_STATE_ERROR
)
from tests.basetestcase import BaseSpreadSheetTestCase
from utils import column_label


class AccessDeniedForNonLoggedinUserTestCase(BaseSpreadSheetTestCase):
    login_as_default_user = False

    def test_denied_list_spreadsheets(self):
        response = self.get("/spreadsheets")
        self.assertEqual(401, response.status_code)
        self.assertNotIn("spreadsheets", response.json)

    def test_denied_spreadsheets_creation(self):
        response = self.post("/spreadsheets/new")
        self.assertEqual(401, response.status_code)

    def test_denied_spreadsheets_get(self):
        response = self.get(f"/spreadsheets/{self.empty_spreadsheet.id}")
        self.assertEqual(401, response.status_code)
        self.assertNotIn("grid", response.json)

    def test_denied_spreadsheets_delete(self):
        response = self.delete(f"/spreadsheets/{self.empty_spreadsheet.id}")
        self.assertEqual(401, response.status_code)

    def test_denied_spreadsheets_update(self):
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {"name": "New name"}
        )
        self.assertEqual(401, response.status_code)
        self.assertNotIn("grid", response.json)


class ListSheetsTestCase(BaseSpreadSheetTestCase):
    def test_list(self):
        expected_response = {'spreadsheets': [
            self.format_spread_sheet(self.empty_spreadsheet),
            self.format_spread_sheet(self.filled_spreadsheet),
        ]}
        response = self.get("/spreadsheets")
        self.assertEqual(200, response.status_code)
        self.assertDictEqual(expected_response, response.json)


class NewSpreadSheetTestCase(BaseSpreadSheetTestCase):
    def test_new_spreadsheet(self):
        response = self.post("/spreadsheets/new")
        self.assertEqual(200, response.status_code)
        self.assertIn("spreadsheet", response.json)

        spreadsheet_id = response.json["spreadsheet"]

        spreadsheet = Spreadsheet.objects.get(id=spreadsheet_id)
        self.assertEqual("New Spreadsheet", spreadsheet.name)


class DeleteSpreadsheet(BaseSpreadSheetTestCase):
    def test_delete(self):
        expected_response = {'spreadsheets': [
            self.format_spread_sheet(self.filled_spreadsheet),
        ]}

        response = self.delete(f"/spreadsheets/{self.empty_spreadsheet.id}")
        self.assertEqual(200, response.status_code)
        self.assertDictEqual(expected_response, response.json)

        # Verify spreadsheet not removed from Database
        affected = Spreadsheet.objects.get(id=self.empty_spreadsheet.id)
        self.assertEqual(SPREADSHEET_STATUS_DELETED, affected.status)

        # Verify other spreadsheets not affected
        other = Spreadsheet.objects.get(id=self.filled_spreadsheet.id)
        self.assertEqual(SPREADSHEET_STATUS_ACTIVE, other.status)

        # Verify deleted spreadsheet removed from listing
        response = self.get("/spreadsheets")
        self.assertEqual(200, response.status_code)
        self.assertDictEqual(expected_response, response.json)

    def test_double_delete_impossible(self):
        response = self.delete(f"/spreadsheets/{self.empty_spreadsheet.id}")
        self.assertEqual(200, response.status_code)
        response = self.delete(f"/spreadsheets/{self.empty_spreadsheet.id}")
        self.assertEqual(421, response.status_code)


class GetSpreadSheetTestCase(BaseSpreadSheetTestCase):
    def test_list_empty_spreadsheet(self):
        # Get spreadsheet and verify it has 'grid'
        response = self.get(f"/spreadsheets/{self.empty_spreadsheet.id}")
        self.assertEqual(200, response.status_code)
        self.assertIn("grid", response.json)
        grid = response.json["grid"]

        # Grid's size is correct
        self.assertEqual(MAX_HEIGHT + 1, len(grid))
        grid.reverse()

        column_head = grid.pop()
        self.assertEqual(MAX_WIDTH + 1, len(column_head))

        # Top left cell should be empty
        column_head.reverse()
        expected_top_left_cell = {"readOnly": True, "value": ''}
        actual_top_left_cell = column_head.pop()
        self.assertDictEqual(expected_top_left_cell, actual_top_left_cell)

        # Verify column heads formatted properly
        for col in range(1, MAX_WIDTH + 1):
            label = column_label(col)
            expected_head = {'value': label, 'readOnly': True}
            actual_head = column_head.pop()
            self.assertDictEqual(expected_head, actual_head)

        # Verify rows
        for row in range(1, MAX_HEIGHT + 1):
            line = grid.pop()
            self.assertEqual(MAX_WIDTH + 1, len(line))

            line.reverse()

            # first cell contains row number
            expected_row_number_cell = {'readOnly': True, 'value': row}
            actual_row_number_cell = line.pop()
            self.assertDictEqual(expected_row_number_cell, actual_row_number_cell)

            # Empty spreadsheet should be rendered into empty values
            expected_line = [{'expr': '', 'value': ''}] * MAX_WIDTH
            self.assertCountEqual(expected_line, line)

    def test_list_with_values(self):
        # filled_spreadsheet has value '1' in 'A1' cell.
        # The rest should be present empty
        # Get spreadsheet and verify it has 'grid'
        response = self.get(f"/spreadsheets/{self.filled_spreadsheet.id}")
        self.assertEqual(200, response.status_code)
        self.assertIn("grid", response.json)
        grid = response.json["grid"]

        # Grid's size is correct
        self.assertEqual(MAX_HEIGHT + 1, len(grid))
        grid.reverse()

        column_head = grid.pop()
        self.assertEqual(MAX_WIDTH + 1, len(column_head))

        # Top left cell should be empty
        column_head.reverse()
        expected_top_left_cell = {"readOnly": True, "value": ''}
        actual_top_left_cell = column_head.pop()
        self.assertDictEqual(expected_top_left_cell, actual_top_left_cell)

        # Verify column heads formatted properly
        for col in range(1, MAX_WIDTH + 1):
            label = column_label(col)
            expected_head = {'value': label, 'readOnly': True}
            actual_head = column_head.pop()
            self.assertDictEqual(expected_head, actual_head)

        # first line contains value in A1
        first_line = grid.pop()
        self.assertEqual(MAX_WIDTH + 1, len(first_line))
        first_line.reverse()
        expected_row_number_cell = {'readOnly': True, 'value': 1}
        actual_row_number_cell = first_line.pop()
        self.assertDictEqual(expected_row_number_cell, actual_row_number_cell)

        expected_a1_value = {'value': 1, 'expr': ''}
        actual_a1_value = first_line.pop()

        self.assertDictEqual(expected_a1_value, actual_a1_value)

        expected_line = [{'expr': '', 'value': ''}] * (MAX_WIDTH - 1)
        self.assertCountEqual(expected_line, first_line)

        # Verify rest of empty cells
        for row in range(2, MAX_HEIGHT + 1):
            line = grid.pop()
            self.assertEqual(MAX_WIDTH + 1, len(line))

            # first cell contains row number
            line.reverse()
            expected_row_number_cell = {'readOnly': True, 'value': row}
            actual_row_number_cell = line.pop()
            self.assertDictEqual(expected_row_number_cell, actual_row_number_cell)

            # Empty spreadsheet should be rendered into empty values
            expected_line = [{'expr': '', 'value': ''}] * MAX_WIDTH
            self.assertCountEqual(expected_line, line)


class SpreadSheetPostRenderingTestCase(BaseSpreadSheetTestCase):
    def test_post_rendering(self):
        # filled_spreadsheet has value '1' in 'A1' cell.
        # The rest should be present empty
        # Get spreadsheet and verify it has 'grid'
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'row': 1, 'col': 1, 'value': '1'}
            ]}
        )
        self.assertEqual(200, response.status_code)
        self.assertIn("grid", response.json)
        grid = response.json["grid"]

        # Grid's size is correct
        self.assertEqual(MAX_HEIGHT + 1, len(grid))
        grid.reverse()

        column_head = grid.pop()
        self.assertEqual(MAX_WIDTH + 1, len(column_head))

        # Top left cell should be empty
        column_head.reverse()
        expected_top_left_cell = {"readOnly": True, "value": ''}
        actual_top_left_cell = column_head.pop()
        self.assertDictEqual(expected_top_left_cell, actual_top_left_cell)

        # Verify column heads formatted properly
        for col in range(1, MAX_WIDTH + 1):
            label = column_label(col)
            expected_head = {'value': label, 'readOnly': True}
            actual_head = column_head.pop()
            self.assertDictEqual(expected_head, actual_head)

        # first line contains value in A1
        first_line = grid.pop()
        self.assertEqual(MAX_WIDTH + 1, len(first_line))
        first_line.reverse()
        expected_row_number_cell = {'readOnly': True, 'value': 1}
        actual_row_number_cell = first_line.pop()
        self.assertDictEqual(expected_row_number_cell, actual_row_number_cell)

        expected_a1_value = {'value': 1, 'expr': ''}
        actual_a1_value = first_line.pop()

        self.assertDictEqual(expected_a1_value, actual_a1_value)

        expected_line = [{'expr': '', 'value': ''}] * (MAX_WIDTH - 1)
        self.assertCountEqual(expected_line, first_line)

        # Verify rest of empty cells
        for row in range(2, MAX_HEIGHT + 1):
            line = grid.pop()
            self.assertEqual(MAX_WIDTH + 1, len(line))

            # first cell contains row number
            line.reverse()
            expected_row_number_cell = {'readOnly': True, 'value': row}
            actual_row_number_cell = line.pop()
            self.assertDictEqual(expected_row_number_cell, actual_row_number_cell)

            # Empty spreadsheet should be rendered into empty values
            expected_line = [{'expr': '', 'value': ''}] * MAX_WIDTH
            self.assertCountEqual(expected_line, line)


class SpreadSheetUpdateTestCase(BaseSpreadSheetTestCase):
    def test_missing_row_returns_error(self):
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'value': '1'}
            ]}
        )
        self.assertEqual(422, response.status_code)

    def test_missing_col_returns_error(self):
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'row': 1, 'value': '1'}
            ]}
        )
        self.assertEqual(422, response.status_code)

    def test_missing_value_returns_error(self):
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 1}
            ]}
        )
        self.assertEqual(422, response.status_code)

    def test_name_update(self):
        expected_name = "Updated name in test_name_update test"
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'name': expected_name}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id = self.empty_spreadsheet.id)
        self.assertEqual(expected_name, spreadsheet.name)

    def test_update_with_value(self):
        expected_value = 7
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 1, 'value': f"{expected_value}"}
            ]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.empty_spreadsheet.id)
        cell = spreadsheet.grid['A1']
        self.assertEqual(expected_value, cell.value)

    def test_update_with_value_and_formula(self):
        expected_value = 2
        expected_formula = "=A1+A2"
        expected_formula_result = 3
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [{'col': 1, 'row': 2, 'value': f"{expected_value}"}]}
        )
        self.assertEqual(200, response.status_code)
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [{'col': 1, 'row': 3, 'value': f"{expected_formula}"}]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.filled_spreadsheet.id)
        cell = spreadsheet.grid['A2']
        self.assertEqual(expected_value, cell.value)
        cell = spreadsheet.grid['A3']
        self.assertEqual(expected_formula_result, cell.value)
        self.assertEqual(expected_formula, cell.expression)

    def test_formula_recalculated_on_value_update(self):
        expected_formula = "=A1+A2"
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [{'col': 1, 'row': 2, 'value': "2"}]}
        )
        self.assertEqual(200, response.status_code)
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [{'col': 1, 'row': 3, 'value': f"{expected_formula}"}]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.filled_spreadsheet.id)
        self.assertEqual(3, spreadsheet.grid['A3'].value)

        # Update A1
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [{'col': 1, 'row': 1, 'value': "5"}]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.filled_spreadsheet.id)
        self.assertEqual(5, spreadsheet.grid['A1'].value)
        self.assertEqual(7, spreadsheet.grid['A3'].value)

        # Update A2
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [{'col': 1, 'row': 2, 'value': "-1"}]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.filled_spreadsheet.id)
        self.assertEqual(-1, spreadsheet.grid['A2'].value)
        self.assertEqual(4, spreadsheet.grid['A3'].value)

    def test_cycles_detection_on_update(self):
        a2_formula = "=A1+A3"
        a3_formula = "=A1+A2"
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 2, 'value': f"{a2_formula}"},
                {'col': 1, 'row': 3, 'value': f"{a3_formula}"},
            ]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.filled_spreadsheet.id)
        cell = spreadsheet.grid['A2']
        self.assertEqual(a2_formula, cell.expression)
        self.assertEqual("#REFF", cell.value)
        cell = spreadsheet.grid['A3']
        self.assertEqual(a3_formula, cell.expression)
        self.assertEqual("#REFF", cell.value)

    def test_cycle_resolved_on_cell_update(self):
        a1_formula = "=B2+1"
        a2_formula = "=A1+1"
        b1_formula = "=A1+2"
        b2_formula = "=B1+A2+2"
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 1, 'value': f"{a1_formula}"},
                {'col': 1, 'row': 2, 'value': f"{a2_formula}"},
                {'col': 2, 'row': 1, 'value': f"{b1_formula}"},
                {'col': 2, 'row': 2, 'value': f"{b2_formula}"},
            ]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.empty_spreadsheet.id)
        for address in ['A1', 'A2', 'B1', 'B2']:
            self.assertEqual("#REFF", spreadsheet.grid[address].value)
            self.assertEqual(CELL_STATE_ERROR, spreadsheet.grid[address].state)

        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 1, 'value': "1"},
            ]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.empty_spreadsheet.id)
        self.assertEqual(1, spreadsheet.grid['A1'].value)
        self.assertEqual(2, spreadsheet.grid['A2'].value)
        self.assertEqual(3, spreadsheet.grid['B1'].value)
        self.assertEqual(7, spreadsheet.grid['B2'].value)

    def test_range_based_formula_get_recalculated_on_dependancy_update(self):
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 1, 'value': "1"},
                {'col': 1, 'row': 2, 'value': "1"},
                {'col': 1, 'row': 3, 'value': f"=SUM(A1:A2)"},
            ]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.empty_spreadsheet.id)
        self.assertEqual(2, spreadsheet.grid['A3'].value)

        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 2, 'value': "2"},
            ]}
        )

        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.empty_spreadsheet.id)
        self.assertEqual(2, spreadsheet.grid['A2'].value)
        self.assertEqual(3, spreadsheet.grid['A3'].value)

    def test_formula_without_addresses(self):
        response = self.post(
            f"/spreadsheets/{self.empty_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 1, 'value': "=1+1"},
            ]}
        )
        self.assertEqual(200, response.status_code)
        spreadsheet = Spreadsheet.objects.get(id=self.empty_spreadsheet.id)
        self.assertEqual(2, spreadsheet.grid['A1'].value)


    def test_formula_is_inproperly_formatted(self):
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 2, 'value': "=ROUNDUP(A1/2)"},
            ]}
        )
        self.assertEqual(200, response.status_code)

    def test_incomplete_formula(self):
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 2, 'value': "=1+A1+"},
            ]}
        )
        self.assertEqual(200, response.status_code)

    def test_empty_formula(self):
        response = self.post(
            f"/spreadsheets/{self.filled_spreadsheet.id}",
            {'changes': [
                {'col': 1, 'row': 2, 'value': "="},
            ]}
        )
        self.assertEqual(200, response.status_code)
