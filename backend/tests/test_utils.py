import unittest

from utils import column_label, coordinates_to_address, address_to_coordinates, \
    IncorrectColumnException, expand_address_range, is_range


class ColumLabelTestCase(unittest.TestCase):
    def test_column_label(self):
        self.assertEqual('A', column_label(1))
        self.assertEqual('B', column_label(2))
        self.assertEqual('CV', column_label(100))
        self.assertEqual('ZZZ', column_label(18278))

    def test_column_label_for_negative_indeces(self):
        self.assertEqual('A', column_label(-1))
        self.assertEqual('A', column_label(-2))
        self.assertEqual('A', column_label(-1000))

    def test_exception_raised_on_out_of_boundaries_input(self):
        with self.assertRaises(IncorrectColumnException):
            column_label(18279)

    def test_exception_raised_for_wrong_input_type(self):
        with self.assertRaises(IncorrectColumnException):
            column_label("A")
        with self.assertRaises(IncorrectColumnException):
            column_label(1.2)


class CoordinatesTestCase(unittest.TestCase):
    def test_corrdiantes_to_key_conversion(self):
        self.assertEqual('A1', coordinates_to_address(1, 1))
        self.assertEqual('CV1', coordinates_to_address(1, 100))
        self.assertEqual('A100', coordinates_to_address(100, 1))
        self.assertEqual('Z26', coordinates_to_address(26, 26))
        self.assertEqual('NTP10000', coordinates_to_address(10000, 10000))

    def test_negative_coordinates(self):
        self.assertEqual('A1', coordinates_to_address(-1, 1))
        self.assertEqual('A1', coordinates_to_address(1, -1))
        self.assertEqual('B1', coordinates_to_address(-2, 2))
        self.assertEqual('A2', coordinates_to_address(2, -2))
        self.assertEqual('AA1', coordinates_to_address(-2, 27))
        self.assertEqual('A27', coordinates_to_address(27, -2))


class AddressToCoordinatesTestCase(unittest.TestCase):
    def test_address_to_coordinates_conversion(self):
        self.assertEqual((1, 1), address_to_coordinates('A1'))
        self.assertEqual((1, 100), address_to_coordinates('CV1'))
        self.assertEqual(((100, 1)), address_to_coordinates('A100'))
        self.assertEqual(((26, 26)), address_to_coordinates('Z26'))
        self.assertEqual((10000, 10000), address_to_coordinates('NTP10000'))


class AddressRangeExpanderTestCase(unittest.TestCase):
    def test_expand_range(self):
        self.assertCountEqual(['A1', 'A2', 'A3'], expand_address_range('A1:A3'))
        self.assertCountEqual(['A1', 'A2', 'A3'], expand_address_range('A3:A1'))

    def test_corners_swap(self):
        self.assertCountEqual(['A1', 'A2', 'B1', 'B2'], expand_address_range('B2:A1'))
        self.assertCountEqual(['A1', 'A2', 'B1', 'B2'], expand_address_range('B1:A2'))


class IsRangeValidatorTestCase(unittest.TestCase):
    def test_is_range_true(self):
        self.assertTrue(is_range("A1:A2"))
        self.assertTrue(is_range("A2:A1"))

    def test_is_range_false(self):
        self.assertFalse(is_range("A1"))
        self.assertFalse(is_range("ZZZ13"))