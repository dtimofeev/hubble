# Features supported

* User signup. In naive way. Any valid email works. No email validation made.
* User login
* User logout
* Spreadsheet creation
* Spreadsheet name update
* Spreadsheet cell update
* Spreadsheet delete
* Cells in the spreadsheet could be either integer value or formulas. 
Details on formulas support are below.
* Cells range addressing in formulas like A1:B3
* State persistence between runs

# Technical background

Backend is written with Python+Flask setup and uses a few popular flask extension. 
Backend uses MongoDB for data persistence through ODB MongoEngine.

Frontend is based on Reac+Redux stack. Spreadsheets component is 
[react-datasheet](https://github.com/nadbm/react-datasheet)
And UI styling provided by [Semantic UI](https://react.semantic-ui.com/).


# Formulas processing

Formulas processed with [pycel](https://github.com/stephenrauch/pycel) library. 
Which is inspired by this [JS version](https://ewbi.blogs.com/develops/2004/12/excel_formula_p.html).
Though many Excel formulas supported only addition is tested and covered with unit-tests.
Formulas work as they work.

# Deploy

Deploy done through Ansible and Docker-Compose and tested on Ubuntu 20.04 LTS.

To deploy a new instance

1. Spin up a new Ubuntu 20.04 instance and ensure you have ssh access to there
1. Put ansible hosts to `./ansible/inventory/` folder
1. Alter `vars/external_vars.yml` file with necessary variables definition
1. Ensure you have docker & docker-compose on your
1. run `./deploy.sh`


# Backend structure

~~~
backend
│   app.py
│   extensions.py
│   utils
│
└───api
│   │   spreadsheet.py
│   │   user.py
│   
└───models
│   │   spreadsheet.py
│   │   user.py
│   
└───schemas
│   
└───venv
~~~

* `app.py` handles flask application setting up
* `extensions.py` takes extensions setting up to avoid cycled imports
* `utils.py` contains utilities for spreadsheet addressing
* `api` folder contains 
*  `models` ODB mappings
* `tests` are for tests

# Frontend structure

~~~
frontend
│   package.json
│   yarn.lock
│
└───public
│   │   index.html
│   │   IQlogo.png
│   
└───src
    │   agent.js
    │   App.js
    │   index.js
    │   localStorage.js
    │   reducer.js
    │   store.js
    │   
    └───components
    │   
    └───constants
    │   │    actionTypes.js
    │   
    └───reducers
~~~

* `index.js` & `App.js` are main application etry points 
*  `agent.js` handles REST requests to the backend
* `reducer.js` set up reducers
* `localStorage.js` & `store.js` handle storage 
* `components` folder contains React components created for the app
* `constants/actionTypes` action types to dispatch and handle
* `reducers` are for reducers

# Deploy structure

~~~
ansible
│   ansible.cfg
│   configure-demo.yml
│   
└───roles
│   │   
│   └───challenge
│   │   
│   └───docker
│   │   
│   └───nginx
│   │   
│   └───ubuntu
│
└───vars
~~~

* `configure-demo.yml` is an Ansible playbook for deploying 
* `roles` ansible roles for playbook
* `vars` variables to alter  

### Deploy roles

* `challenge` uploads this challenge to configured hosts
* `docker` installs `docker` & `docker-compose`
* `nginx` installs Nginx reverse proxy server and configures it 
to server both the backend and frontend
* `ubuntu` updates apt packages cache